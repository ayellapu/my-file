import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
public class UpdateData {
	@SuppressWarnings("unchecked")
	public void updateData() throws IOException {
		File file=new File("C:\\Users\\ayellapu\\eclipse-workspace\\File IO\\src\\JsonFile.json");
		FileWriter filewriter=new FileWriter (file);
		JSONObject firstuser = new JSONObject();
    	firstuser .put("firstName", "anil");
    	firstuser .put("age", "23");
    	firstuser .put("designation", "IT emplyee");
    	firstuser .put("location", "vizag");
    	JSONArray listofhobbies=new JSONArray();
    	listofhobbies.add("cricket");
    	listofhobbies.add("eating");
    	firstuser.put("hobbies",listofhobbies);
     
    	JSONObject userObject = new JSONObject(); 
    	userObject.put("user", firstuser );
     
    	//Second user
    	JSONObject seconduser = new JSONObject();
    	seconduser.put("firstName", "kumar");
    	seconduser.put("age", "25");
    	seconduser.put("designation", "electrical engineer");
    	JSONArray listofhobbies2=new JSONArray();
    	listofhobbies2.add("running");
    	listofhobbies2.add("games");
    	seconduser.put("hobbies",listofhobbies2);
     
    	JSONObject userObject2 = new JSONObject(); 
    	userObject2 .put("user", seconduser);
     
    	//Add users to list
    	JSONArray userList = new JSONArray();
    	userList.add(userObject);
    	userList.add(userObject2);
     
    	//Write JSON file
    	filewriter.write( userList.toJSONString()); 
    	filewriter.flush();
    	filewriter.close();
    	}
	}


