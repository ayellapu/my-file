import java.io.IOException;
import java.util.Scanner;
	/**
	 * This is a class to implement the file operations
	 * @author ayellapu1
	 *
	 */
public class File {
		public static void main(String[] args) throws IOException {
			//printing the menu
			System.out.println("THESE ARE FILE IO OPERATIONS");
			System.out.println("----------------------------");
			System.out.println("1.CREATE A NEW FILE");
			System.out.println("2.APPEND DATA TO THE NEW FILE");
			System.out.println("3.UPDATE NEW DATE TO THE FILE");
			System.out.println("4.DELETE THE DATA FROM THE FILE");
			System.out.println("5.DELETE THE FILE");
			System.out.println("Enter your choice");
			Scanner sc=new Scanner(System.in);
			int ch=sc.nextInt();
					switch(ch) {
			case 1:
					//create a new file
				 	CreateNewFile createfile=new CreateNewFile();
				 	createfile.fileCreate();
				 	System.out.println("file created");
				 break;
			case 2:
				 	//First user
		        	UpdateData updatedata=new UpdateData();
		        	updatedata.updateData();
		        	System.out.println("data updated");
		        	
				break;
			}
					sc.close();
			
		}
	}



